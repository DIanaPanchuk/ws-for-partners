$(function(){
	$('body').on('click', function() {
		$('.humburger-menu').removeClass('open');
		$('#menu').removeClass('open');
		$('body').removeClass('open');
	});
	$('.humburger-menu').on('click', function(e) {
		e.stopPropagation();
		$('.humburger-menu').toggleClass('open');
		$('#menu').toggleClass('open');
		$('body').toggleClass('open');
	});

	$(".js-form").on('submit', function(e){
		e.preventDefault();
		sendForm();
	});

	$('.js-ok').on('click', function(){
		$('.js-form')[0].reset();
		$(this).parent().addClass('hidden');
	});
});
function sendForm() {
	$(".js-form").validate({
		rules: {
			name: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			phone: {
				required: true,
				number: true
			}
		},
		messages: {
			name: {
				required: "Поле не должно быть пустым",
				minlength: "Поле Имя слишком короткое"
			},
			email: {
				required: "Поле не должно быть пустым",
				email: "Некорректный формат"
			},
			phone: {
				required: "Поле не должно быть пустым",
				numbers: "Некорректный формат"
			}
		},
		submitHandler: function() {
			var data = {
				"message": {
					name: $("#name").val(),
					email: $("#email").val(),
					message: $("#phone").val() + " сообщение отправлено с формы на " + location
				}
			};
			console.log(data);
			$.ajax({
				url: 'http://api.web-systems.solutions/api/messages',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(resp) {
					console.log(resp);
					$('.success').removeClass('hidden');
				}
			});
		}
	});
}
